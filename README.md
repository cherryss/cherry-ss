# CherrySS

#### 介绍
实现一个命令行文本计数统计程序。能正确统计导入的纯英文txt文本中的字符数，单词数，句子数。\



#### 使用方法
命令模式： wc.exe - file.txt a

例：wc.exe - file.txt a 统计字符数\
例：wc.exe - file.txt b 统计单词数\
例：wc.exe - file.txt c 统计句子数


